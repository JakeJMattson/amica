package me.aberrantfox.boardbot.configuration

import me.aberrantfox.kjdautils.api.annotation.Data

@Data("config/config.json")
data class ProjectConfiguration(
        val ownerID: String = "insert-id",
        var timerInterval: Int = 10,
        val notifyMessages: Boolean = true,
        val notifyCategoryList: ArrayList<String> = ArrayList(),
        val notifyVoiceChannels: ArrayList<String> = ArrayList(),
        val ignoredChannels: ArrayList<String> = ArrayList(),
        val boardMessages: ArrayList<MessageLog> = ArrayList()
)

data class MessageLog(val channelId: String, val messageId: String)
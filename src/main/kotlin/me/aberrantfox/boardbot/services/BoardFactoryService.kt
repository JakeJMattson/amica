package me.aberrantfox.boardbot.services

import me.aberrantfox.boardbot.configuration.ProjectConfiguration
import me.aberrantfox.kjdautils.api.annotation.Service
import me.aberrantfox.kjdautils.api.dsl.embed
import me.aberrantfox.kjdautils.extensions.jda.fullName
import net.dv8tion.jda.core.JDA
import net.dv8tion.jda.core.entities.Member
import net.dv8tion.jda.core.entities.TextChannel
import java.awt.Color

@Service
class BoardFactoryService(val configuration: ProjectConfiguration, val jda: JDA) {
    fun build() = embed {
        title("Information Board")
        description("Below you can see important information and tracked events.")
        color(Color.RED)
        configuration.notifyCategoryList.mapNotNull { jda.getCategoryById(it) }.forEach { category ->
            val channels = category.textChannels.filterNot { configuration.ignoredChannels.contains(it.id) }
            val cname = category.name

            field {
                name = cname
                value = buildChannelString(channels)
                inline = false
            }
        }

        configuration.notifyVoiceChannels.mapNotNull { jda.getVoiceChannelById(it) }.forEach { channel ->
            val currentMembers = channel.members
            val maxSize = channel.userLimit

            val userLimitString = if(channel.userLimit == 0) currentMembers.size else "${currentMembers.size}/$maxSize"

            field {
                name = "${channel.name} ($userLimitString)"
                value = buildVoiceChannelString(currentMembers)
            }
        }
    }

    private fun buildChannelString(channels: List<TextChannel>): String {
        if(channels.isEmpty()) {
            return "No tracked channels to display."
        }

        val builder = StringBuilder()
        channels.forEach { channel ->
            builder
                    .append(channel.name)
                    .append(" :: ")
                    .append(channel.asMention)
                    .appendln()
        }

        return builder.toString()
    }

    private fun buildVoiceChannelString(members: List<Member>): String {
        if(members.isEmpty()) {
            return "No members present."
        }
        val builder = StringBuilder()
        members.forEach { member -> builder.append(member.fullName()).appendln() }

        return builder.toString()
    }
}
package me.aberrantfox.boardbot.services

import me.aberrantfox.boardbot.configuration.ProjectConfiguration
import me.aberrantfox.kjdautils.api.annotation.Service
import net.dv8tion.jda.core.JDA
import net.dv8tion.jda.core.entities.Message
import java.util.*
import kotlin.concurrent.schedule

@Service
class UpdateService(val configuration: ProjectConfiguration, val jda: JDA, val embedService: BoardFactoryService) {
    init {
        beginUpdates()
    }

    fun beginUpdates() {
        Timer().schedule((configuration.timerInterval * 1000).toLong()) {
            val boardPairs = configuration.boardMessages.map { Pair(jda.getTextChannelById(it.channelId), it.messageId) }

            boardPairs.forEach { pair ->
                pair.first.getMessageById(pair.second).queue {
                    sendUpdate(it)
                }
            }

            beginUpdates()
        }
    }

    fun sendUpdate(message: Message) = message.editMessage(embedService.build()).queue()
}
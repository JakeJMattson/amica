package me.aberrantfox.boardbot.preconditions

import me.aberrantfox.boardbot.configuration.ProjectConfiguration
import me.aberrantfox.kjdautils.api.dsl.Precondition
import me.aberrantfox.kjdautils.api.dsl.precondition
import me.aberrantfox.kjdautils.internal.command.Fail
import me.aberrantfox.kjdautils.internal.command.Pass

@Precondition
fun isOwnerPrecondition(configuration: ProjectConfiguration) = precondition {
    if(it.author.id == configuration.ownerID) {
        return@precondition Pass
    } else {
        return@precondition Fail("")
    }
}
#!/bin/bash
docker build -t boardbot:latest -f docker/Dockerfile .
docker run -e BOT_TOKEN=$1 -v $2:/config boardbot:latest
